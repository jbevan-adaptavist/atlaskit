module.exports = {
  // Relative to this directory
  srcDir: '../icons',
  processedDir: '../icons/processed',
  destDir: '../glyph',
  maxWidth: 24,
  maxHeight: 24,
};
