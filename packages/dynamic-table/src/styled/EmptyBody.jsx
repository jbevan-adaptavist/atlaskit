import styled from 'styled-components';

export default styled.div`
  margin: auto;
  padding: 10px;
  text-align: center;
  width: 50%;
`;
