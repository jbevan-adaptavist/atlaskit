import React from 'react';
import Calendar from '@atlaskit/calendar';

const CalendarExample = () => (
  <Calendar onUpdate={console.log} />
);

export default CalendarExample;
