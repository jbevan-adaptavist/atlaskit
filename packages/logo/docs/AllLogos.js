import React from 'react';
import {
  AtlassianLogo,
  BitbucketLogo,
  ConfluenceLogo,
  HipchatLogo,
  JiraLogo,
  JiraCoreLogo,
  JiraServiceDeskLogo,
  JiraSoftwareLogo,
  StatuspageLogo,
  StrideLogo,
} from '@atlaskit/logo';

const Examples = () => (
  <div>
    <AtlassianLogo />
    <BitbucketLogo />
    <ConfluenceLogo />
    <HipchatLogo />
    <JiraLogo />
    <JiraCoreLogo />
    <JiraServiceDeskLogo />
    <JiraSoftwareLogo />
    <StatuspageLogo />
    <StrideLogo />
  </div>
);

export default Examples;
