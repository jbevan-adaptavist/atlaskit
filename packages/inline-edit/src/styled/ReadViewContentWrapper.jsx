import styled from 'styled-components';

const ReadViewContentWrapper = styled.div`
  display: flex;
  flex: 0 0 auto;
  min-width: 0;
  width: 100%;
`;

ReadViewContentWrapper.displayName = 'ReadViewContentWrapper';

export default ReadViewContentWrapper;
