const path = require('path');

module.exports = [
  { name: 'Flag', src: path.join(__dirname, '../src/components/Flag.jsx') },
  { name: 'AutoDismissFlag', src: path.join(__dirname, '../src/components/AutoDismissFlag.jsx') },
  { name: 'FlagGroup', src: path.join(__dirname, '../src/components/FlagGroup.jsx') },
];
