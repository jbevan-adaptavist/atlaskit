/**
 * This file is used in development with lerna
 * Renderer is not exported from the editor-core because of BB
 * which is using old webpack version at the moment.
 */
export * from '../../../src/renderer';
