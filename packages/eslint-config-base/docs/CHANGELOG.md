# @atlaskit/eslint-config-base

## 2.0.7 (2017-08-11)




* bug fix; relax arrow-parens rule for better compatibilty with prettier ([0823593](https://bitbucket.org/atlassian/atlaskit/commits/0823593))

## 2.0.6 (2017-07-25)


* fix; use class transform in loose mode in babel to improve load performance in apps ([fde719a](https://bitbucket.org/atlassian/atlaskit/commits/fde719a))

## 2.0.5 (2017-05-06)


* fix; update eslint config to use javascript, .eslintrc is deprecated ([ce7a5c5](https://bitbucket.org/atlassian/atlaskit/commits/ce7a5c5))

## 2.0.4 (2017-03-24)


* fix; adding missing browserslist file for eslint-plugin-compat ([6e10362](https://bitbucket.org/atlassian/atlaskit/commits/6e10362))

## 2.0.2 (2017-03-21)

## 2.0.2 (2017-03-21)


* fix; maintainers for all the packages were added ([261d00a](https://bitbucket.org/atlassian/atlaskit/commits/261d00a))

## 2.0.1 (2017-02-27)


* empty commit to make components release themselves ([5511fbe](https://bitbucket.org/atlassian/atlaskit/commits/5511fbe))
