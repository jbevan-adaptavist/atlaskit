# Layer Manager

The layer manager is used to render React DOM into a new context (aka "Portal"). This can be used to implement various UI components such as modals.

## Try it out

Interact with a [live demo of the @NAME@ component](https://aui-cdn.atlassian.com/atlaskit/stories/@NAME@/@VERSION@/).

## Installation

```sh
npm install @NAME@
```
